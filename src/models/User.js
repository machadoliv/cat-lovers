const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const User = sequelize.define('User', {
  name: {
      type: DataTypes.STRING,
      allowNull: false
  },
  email: {
      type: DataTypes.STRING,
      allowNull: false
  },
  birthday: {
      type: DataTypes.DATEONLY,
      allowNull: false
  },

  phone: {
      type: DataTypes.STRING,
      allowNull: false
  },
  cpf: {
      type: DataTypes.STRING,
      allowNull: false
  },
  hash: {
      type: DataTypes.STRING,
  },
  salt: {
      type: DataTypes.STRING,
  }

});


User.associate = function (models) {
  User.belongsToMany(models.CatImage, {
    through: 'Favorites',
    as: 'Favoriting'
  })
}

module.exports = User;

  