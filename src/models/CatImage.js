const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');



const CatImage = sequelize.define("Cat",
{
    id: {
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false,
    },
    url: {
        type: DataTypes.STRING,
        allowNull: false,        
    },
    width : {
        type: DataTypes.INTEGER,
    },
    height : {
        type: DataTypes.INTEGER
    },

})

CatImage.associate = function (models) {
    CatImage.belongsToMany(models.User, {
      through: 'Favorites',
      as: 'Favoriting'
    })
  }
module.exports = CatImage;