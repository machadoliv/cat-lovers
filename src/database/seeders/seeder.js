require('../../config/dotenv')();
require('../../config/sequelize');

const catImageSeeder = require('./catImageSeeder');
const UserSeeder = require('./UserSeeder');

(async () => {
  try {
    await catImageSeeder();
    await UserSeeder();
    
  } catch(err) { console.log(err) }
})();

