const CatImage = require("../../models/CatImage");
const axios = require("axios");

const catImageSeeder = async function () {
    const ImageArray = [];
  try {
    await axios.get("https://api.thecatapi.com/v1/images/search?limit=10").then((response)=>{
        // console.log(response.data);
        for (let i=0; i<10 ; i++){

            ImageArray.push({
            id: response.data[i].id,
            url: response.data[i].url,
            width: response.data[i].width,
            height: response.data[i].height,
            });
        }
    });
    await CatImage.sync({ force: true });
    await CatImage.bulkCreate(ImageArray)
    
  } catch (err) {
    console.log(err);
}
};

module.exports = catImageSeeder;
