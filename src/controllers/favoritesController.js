const User = require('../models/User');
const CatImage = require('../models/CatImage');

//adiciona aos favoritos

const addImagesInFavorites = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const catImage = await CatImage.findByPk(req.body.id);
        await user.addFavoriting(catImage);
        return res.status(200).json('Produto adicionado aos favoritos!');
    }catch(err){
        return res.status(500).json(err+'');
    }
};

//remove dos favoritos

const removeImagesfromFavorites = async(req,res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const catImage = await CatImage.findByPk(req.body.id);
        await user.removeFavoriting(catImage);
        return res.status(200).json('Produto removido dos favoritos!');
    }catch(err){
        return res.status(500).json({err});
    }
};

//lista os produtos que estão nos favoritos

const listFavorites = async(req, res) => {
    const {id} = req.params;
    try{
        const user = await User.findByPk(id);
        const list = await user.getFavoriting();
        return res.status(200).json({ list });
    }
    catch(err) {
        return res.status(500).json(err+'');
    }
};

module.exports ={
    addImageInFavorites,
    removeImagefromFavorites,
    listFavorites
}
