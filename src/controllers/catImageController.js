const CatImage= require ("./../models/CatImage");

//mostrar todos as imagens de gato
async function index(req, res) {
    try {
        const catImage = await CatImage.findAll();
        return res.status(200).json({ catImage });
    }
    catch (err) {
        return res.status(500).json({ err });
    }
}
//filtrar uma foto por id
async function show(req, res) {
    const { id } = req.params;
    try {
        const catImage = await CatImage.findByPk(id);
        return res.status(200).json({ catImage })
    }
    catch (err) {
        return res.status(500).json("Imagem não encontrado")
    }
}

//criar uma nova imagem 
async function create(req, res) {
    try {
        const newCatImage = {
            id: req.body.id,
            url: req.body.url,
            width: req.body.width,
            height:req.body.height,
        };
        const catImage = await CatImage.create(newCatImage);
        return res.status(201).json("Sua foto foi registrada com sucesso.");
    }
    catch (err) {
        return res.status(500).json({ err })
    }
}

//deleta imagem
async function destroy(req, res) {
    const { id } = req.params;
    try {
        const deleted = await CatImage.destroy({ where: { id: id } })
        if (deleted) {
            return res.status(202).json("Foto deletada :(")
        }
    }
    catch (err) {
        return res.status(500).json({ err });
    }
}

module.exports = {
    create, show, index, destroy
}


export default catImageController;