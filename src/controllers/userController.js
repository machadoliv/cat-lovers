import User from "../models/User";

//mostrar todos os Usuários
async function index(req, res) {
    try {
        const users = await User.findAll();
        return res.status(200).json({ users });
    }
    catch (err) {
        return res.status(500).json({ err });
    }
}
//filtrar um Usuário por id
async function show(req, res) {
    const { id } = req.params;
    try {
        const trainer = await User.findByPk(id);
        return res.status(200).json({ trainer })
    }
    catch (err) {
        return res.status(500).json("Usuário não encontrado")
    }
}

//criar perfil de Usuário
async function create(req, res) {
    try {
        const { password } = req.body;
        const HashSalt = Auth.generatePassword(password);
        const salt = HashSalt.salt;
        const hash = HashSalt.hash;
        const newUser = {
             name: req.body.name,
            email: req.body.email,
            birthday: req.body.birthday,
            phone: req.body.phone,
            cpf: req.body.cpf,
            type_user: req.body.type_user,
            hash: hash,
            salt: salt
        };
        const user = await User.create(newUser);
        return res.status(201).json("Usuário registrado com sucesso.");
    }
    catch (err) {
        return res.status(500).json({ err })
    }
}
//atualizar alguma instância do Usuário
async function update(req, res) {
    const { id } = req.params;
    try {
        const [updated] = await User.update(req.body, { where: { id: id } });
        if (updated) {
            const user = await User.findByPk(id);
            return res.status(202).send(user);
        }
    }
    catch (err) {
        return res.status(500).json({ err })
    }
}
//deleta o Usuário
async function destroy(req, res) {
    const { id } = req.params;
    try {
        const deleted = await User.destroy({ where: { id: id } })
        if (deleted) {
            return res.status(202).json("Usuário deletado :(")
        }
    }
    catch (err) {
        return res.status(500).json({ err });
    }
}

module.exports = {
    create, show, index, update, destroy
}


export default UserController;