
const { Router } = require('express');
const UserController = require('../controllers/userController')
const catImageController = require('../controllers/catImageController')
const favoritesController = require('../controllers/favoritesController')

//authentication uses
const AuthController = require("../controllers/AuthController");
const passport = require("passport");
router.use("/private", passport.authenticate('jwt', {session: false}));

//authentication routes
router.post("/login", AuthController.login);
router.get("/private/getDetails", AuthController.getDetails);

//User Routes
router.get('/users', UserController.index);
router.get('/private/users/:id', UserController.show);
router.post('/users',  UserController.create);
router.put('/private/users/:id',  UserController.update);
router.delete('/private/users/:id', UserController.destroy);

//CatImage Routes

router.get('/cat-image', catImageController.index);
router.get('/private/cat-image/:id', UserController.show);
router.post('/cat-image',  UserController.create);
router.delete('/private/cat-image/:id', UserController.destroy);

//Favorites Routes

router.put('/users/addinfavorites/:id', favoritesController.addImagesInFavorites);
router.put('/users/removefromfavorites/:id', favoritesController.removeImagesfromFavorites);
router.get('/users/listinfavorites/:id', favoritesController.listFavorites);

