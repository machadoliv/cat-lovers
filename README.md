# Cat Lovers | EJCM
Projeto feito para o processo seletivo interno para o cargo de tech lead feito em Node.js e Express contendo CRUD básica de duas entidades relacionadas entre si e consumindo uma API pública - ![The Cat Api](https://thecatapi.com/)
 
**Status do Projeto** : Incompleto

![Badge](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)
![Badge](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)
 
 
## Tabela de Conteúdo

*Faça um índice com links internos para todos os tópicos seguintes.*

 1. [Tecnologias utilizadas](#tecnologias-utilizadas)
 2. [Instalação](#instalação)
 3. [Configuração](#configuração)
 4. [Uso](#uso)
 5. [Arquitetura](#arquitetura)
 6. [Autores](#autores)
 
## Tecnologias utilizadas

Essas são as frameworks e ferramentas que você precisará instalar para desenvolver esse projeto:

 - [Node](https://nodejs.org/en/) v16.15.1
 - [Sequelize](https://sequelize.org/) v6.21.4  
 - [Express](http://expressjs.com/pt-br/) v4.18.1


## Instalação 

Na raiz do projeto, basta utilizar o comando

``` bash
$ npm install
```

## Configuração

Veremos a seguir os comandos necessários para configurar o projeto.

Primeiramente devemos fazer a cópia do arquivo .env

``` bash
$ cp .env.example .env
```

E então, devemos criar o banco e popula-lo

``` bash
$ npm run migrate
```
``` bash
$ npm run seed
```
 
## Uso

Após realizar os passos de configuração, basta digitar o comando a seguir para rodar a API

``` bash
$ npm run dev
```
## Arquitetura

![image.png](./image.png)

## Autores

Lívia Machado Campos  
 

## Última atualização: 23/09/2022

